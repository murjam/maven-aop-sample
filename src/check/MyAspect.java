package check;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class MyAspect {

	@Before("execution(* getList())")
	public void nothing(JoinPoint jp) {
		System.out.println(jp.getSignature());
	}
}
