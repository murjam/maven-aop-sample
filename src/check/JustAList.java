package check;

import java.util.ArrayList;

public class JustAList {
	
	public static ArrayList<String> getList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Mati");
		list.add("Tom");
		return list;
	}

	public static void main(String[] args) {
		System.out.println(getList());
	}

}
